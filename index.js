const express = require('express');
const app = express();
const http = require('http').Server(app)
const io = require('socket.io')(http);

const apiai = require('apiai')('128a9d5a6a98427ba480a072efd1b915');



app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));


http.listen(3000, function(){
  console.log('server started on *3000');
});

io.on('connection', function(socket){
  console.log('user has connected');

  socket.emit('test', 'you are connected');
  socket.on('new message', (msg) =>{

    console.log(msg);

    // Get a reply from API.AI

    let apiaiReq = apiai.textRequest(msg, {
      sessionId: '128a9d5a6a98427ba480a072efd1b915'
    });

    apiaiReq.on('response', (response) => {
      console.log(response);
      let aiText = response.result.fulfillment.speech;
      socket.emit('bot reply', aiText); // Send the result back to the browser!
    });

    apiaiReq.on('error', (error) => {
      console.log(error);
    });

    apiaiReq.end();


  });
});


app.get('/', (req, res) =>{
  res.sendFile(__dirname + '/views/index.html');
});
