let socket = io();

//setup speechSynthesis
const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();
recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.addEventListener('result', (e) => {
  let last = e.results.length - 1;
  let text = e.results[last][0].transcript;

  showMessage('You' , e.results[0][0].transcript);
  console.log(e.results[0][0].transcript);
  console.log('Confidence: ' + e.results[0][0].confidence);

  socket.emit('new message', e.results[0][0].transcript);

});


//hook up talk btn.
document.querySelector('#talk').addEventListener('click', () => {
  console.log('speechRecoStarted');
  recognition.start();
});


//websocket
socket.on('test', (message) =>{
  console.log(message);
});

socket.on('bot reply', (res) =>{
  console.log(res);
  showMessage('Bot', res);
  voice(res);
});

//functions
let showMessage = function(who,msg){
  let p = document.createElement('p');
  p.innerHTML = who + ' said: ' + msg;
  document.querySelector('#messages').appendChild(p);
}

let voice = function (text) {
  console.log('shoud speak' + text);
  const synth = window.speechSynthesis;
  const utterance = new SpeechSynthesisUtterance();
  utterance.text = text;
  utterance.lang = 'en-Us';
  synth.speak(utterance);
  console.log(synth);
}
